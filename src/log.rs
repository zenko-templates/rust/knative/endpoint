use tracing_subscriber::filter::ParseError;
use tracing_subscriber::prelude::__tracing_subscriber_SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;
use tracing_subscriber::{fmt, EnvFilter, Layer, Registry};

use crate::settings::log::LogSetting;

pub fn init_tracing(config: LogSetting) -> Result<(), ParseError> {
    let filter: EnvFilter = EnvFilter::default()
        .add_directive(format!("service::app={:?}", config.app).parse()?)
        .add_directive(format!("[request]={:?}", config.request).parse()?);

    let layer = fmt::layer().with_filter(filter);

    Registry::default().with(layer).init();

    Ok(())
}
