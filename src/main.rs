pub mod app;
pub mod entities;
pub mod errors;
pub mod extractors;
pub mod log;
pub mod repositories;
pub mod services;
pub mod settings;
pub mod utils;

use app::App;

#[tokio::main]
async fn main() {
    let mut app = App::new().expect("Cannot create App");

    app.start().await.expect("App Crashed");
}
