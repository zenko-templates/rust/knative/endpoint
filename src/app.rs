pub mod errors;
pub mod state;

use std::{
    net::{IpAddr, SocketAddr},
    sync::Arc,
};

use tracing::{error, info};

use crate::{log::init_tracing, services::create_services, settings::Setting};

use self::{errors::AppError, state::AppStateBuilder};

pub struct App {
    config: Setting,
    state_builder: AppStateBuilder,
}

impl App {
    pub fn new() -> Result<App, AppError> {
        let config = Setting::init().map_err(|err| {
            println!("Failed to initialize configuration. Err: {}", err);
            AppError::InvalidConfig
        })?;

        Ok(App {
            config,
            state_builder: AppStateBuilder::default(),
        })
    }

    async fn init(&mut self) -> Result<(), AppError> {
        init_tracing(self.config.log.clone()).map_err(|err| {
            error!("Failed to initialize tracing. Err: {}", err);
            AppError::FailToInitTracing
        })?;

        #[cfg(feature = "redis")]
        self.state_builder
            .with_redis(self.config.database.redis.clone())
            .map_err(|err| {
                error!("Failed to initialize redis client. Err: {}", err);
                AppError::FailedToInitRedisClient
            })?;

        #[cfg(feature = "mongo")]
        self.state_builder
            .with_mongo(self.config.database.mongo.clone())
            .await
            .map_err(|err| {
                error!("Failed to initialize mongo client. Err: {}", err);
                AppError::FailedToInitMongoClient
            })?;

        #[cfg(feature = "http_client")]
        self.state_builder
            .with_http_client(self.config.http_client.clone())
            .await
            .map_err(|err| {
                error!("Failed to initialize http client. Err: {}", err);
                AppError::FailedToInitHttpClient
            })?;

        Ok(())
    }

    pub async fn start(&mut self) -> Result<(), AppError> {
        self.init().await?;

        let state = self
            .state_builder
            .build(self.config.clone())
            .map_err(|err| {
                error!("Failed to initialize state. Err: {}", err);
                AppError::FailedToInitState
            })?;

        let service = create_services(Arc::new(state));

        let ip: IpAddr = self.config.app.host.clone().parse().map_err(|err| {
            error!("Cannot parse host. Err: {}", err);
            AppError::InvalidHost
        })?;
        let addr = SocketAddr::new(ip, self.config.app.port);

        info!("listening on {}", addr);

        axum::Server::bind(&addr)
            .serve(service)
            .await
            .map_err(|err| {
                error!("App crashed. Err: {}", err);
                AppError::Crashed
            })?;

        Ok(())
    }
}
