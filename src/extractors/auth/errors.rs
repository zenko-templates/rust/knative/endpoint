use axum::http::StatusCode;

pub enum AuthErrors {
    AuthenticationHeaderNotFound,
    InvalidHeader,
    CannotDecodeValue,
    InvalidValue,
    EmptyValue,
    ExpiredValue,
}

impl From<AuthErrors> for StatusCode {
    fn from(value: AuthErrors) -> Self {
        return StatusCode::UNAUTHORIZED;
    }
}
