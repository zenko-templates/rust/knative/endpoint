use std::sync::Arc;

use axum::{
    async_trait,
    extract::FromRequestParts,
    http::{request::Parts, StatusCode},
};
use tracing::error;

use crate::{
    app::state::AppState,
    extractors::auth::errors::AuthErrors,
    settings::{auth::AuthSetting, Setting},
};

pub struct ApiKey(pub String);

#[async_trait]
impl FromRequestParts<Arc<AppState>> for ApiKey {
    type Rejection = StatusCode;

    async fn from_request_parts(
        parts: &mut Parts,
        state: &Arc<AppState>,
    ) -> Result<Self, Self::Rejection> {
        let Setting {
            auth:
                AuthSetting {
                    api_key_header,
                    api_key,
                    ..
                },
            ..
        } = state.config.clone();

        let current_api_key = match parts.headers.get(api_key_header) {
            Some(header) => header
                .to_str()
                .map_err(|_| AuthErrors::AuthenticationHeaderNotFound),
            None => {
                error!("No API KEY header found");
                Err(AuthErrors::AuthenticationHeaderNotFound)
            }
        }?;

        if api_key != current_api_key {
            error!("Invalid API KEY");
            Err(AuthErrors::InvalidValue)?;
        }

        Ok(ApiKey(current_api_key.to_string()))
    }
}
