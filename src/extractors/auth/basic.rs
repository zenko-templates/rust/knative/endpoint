use axum::{
    async_trait,
    extract::FromRequestParts,
    http::{header, request::Parts, StatusCode},
};
use base64::{engine::general_purpose, Engine};
use std::sync::Arc;
use tracing::error;

use crate::app::state::AppState;

use super::{errors::AuthErrors, AuthUser};

#[derive(Debug)]
pub struct Basic(pub AuthUser);

#[async_trait]
impl FromRequestParts<Arc<AppState>> for Basic {
    type Rejection = StatusCode;

    async fn from_request_parts(
        parts: &mut Parts,
        state: &Arc<AppState>,
    ) -> Result<Self, Self::Rejection> {
        let authorization = match parts.headers.get(header::AUTHORIZATION) {
            Some(header) => header
                .to_str()
                .map_err(|_| AuthErrors::AuthenticationHeaderNotFound),
            None => {
                error!("No authorization header found");
                Err(AuthErrors::AuthenticationHeaderNotFound)
            }
        }?;

        let encoded_credentials = match authorization.split_once(' ') {
            Some((key, credential)) if key == "Basic" => Ok(credential),
            _ => {
                error!("Authorization is not of type Basic");
                Err(AuthErrors::InvalidHeader)
            }
        }?;

        let bytes = general_purpose::URL_SAFE
            .decode(encoded_credentials)
            .map_err(|err| {
                error!("Cannot decode credentials Err: {}", err);
                AuthErrors::CannotDecodeValue
            })?;
        let decoded_credentials = String::from_utf8(bytes).map_err(|err| {
            error!("Cannot decode credentials Err: {}", err);
            AuthErrors::CannotDecodeValue
        })?;

        let (_username, _password) = match decoded_credentials.split_once(':') {
            Some((username, password)) => Ok((username, password)),
            None => {
                error!("No username or password provided");
                Err(AuthErrors::EmptyValue)
            }
        }?;

        let user = AuthUser {
            id: String::from("Placeolder"),
        };

        Ok(Basic(user))
    }
}
