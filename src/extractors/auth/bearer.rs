use std::{
    sync::Arc,
    time::{SystemTime, UNIX_EPOCH},
};

use axum::{
    async_trait,
    extract::FromRequestParts,
    http::{header, request::Parts, StatusCode},
};
use serde::Deserialize;
use tracing::error;

use crate::{
    app::state::AppState,
    settings::{auth::AuthSetting, Setting},
    utils,
};

use super::{errors::AuthErrors, AuthUser};

#[derive(Deserialize)]
pub struct Claims {
    _iss: String,
    sub: String,
    _aud: String,
    exp: u128,
    _iat: u128,
}

#[derive(Debug)]
pub struct Bearer(pub AuthUser);

#[async_trait]
impl FromRequestParts<Arc<AppState>> for Bearer {
    type Rejection = StatusCode;

    async fn from_request_parts(
        parts: &mut Parts,
        state: &Arc<AppState>,
    ) -> Result<Self, Self::Rejection> {
        let authorization = match parts.headers.get(header::AUTHORIZATION) {
            Some(header) => header
                .to_str()
                .map_err(|_| AuthErrors::AuthenticationHeaderNotFound),
            None => {
                error!("Not authorization header found");
                Err(AuthErrors::AuthenticationHeaderNotFound)
            }
        }?;

        let token_str = match authorization.split_once(' ') {
            Some((key, token)) if key == "Bearer" => Ok(token),
            _ => {
                error!("Authorization is not of type Bearer");
                Err(AuthErrors::InvalidHeader)
            }
        }?;

        let Setting {
            auth: AuthSetting { public_key, .. },
            ..
        } = state.config.clone();

        let claims =
            utils::jwt::decode::<Claims>(public_key, token_str.to_string()).map_err(|err| {
                error!("Cannot decode token Err: {}", err);
                AuthErrors::CannotDecodeValue
            })?;

        // Check if the exp is not passed
        let now = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_millis();
        if claims.exp <= now {
            error!("Token expired");
            Err(AuthErrors::ExpiredValue)?;
        };

        // Create the authenticated user
        let user = AuthUser { id: claims.sub };

        Ok(Bearer(user))
    }
}
