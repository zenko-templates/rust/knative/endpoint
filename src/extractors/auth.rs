#[cfg(feature = "api_key_auth")]
pub mod api_key;
#[cfg(feature = "basic_auth")]
pub mod basic;
#[cfg(feature = "bearer_auth")]
pub mod bearer;
pub mod errors;

#[derive(Debug, Clone)]
pub struct AuthUser {
    pub id: String,
}
