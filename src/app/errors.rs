use thiserror::Error;

#[derive(Debug, Error)]
pub enum AppError {
    #[error("Invalid config")]
    InvalidConfig,

    #[error("Invalid host")]
    InvalidHost,

    #[error("Fail to init tracing")]
    FailToInitTracing,

    #[error("App Crashed")]
    Crashed,

    #[error("Failed to initialize redis client")]
    FailedToInitRedisClient,

    #[error("Failed to initialize mongo client")]
    FailedToInitMongoClient,

    #[error("Failed to initialize http client")]
    FailedToInitHttpClient,

    #[error("Failed to initialize state")]
    FailedToInitState,
}

#[derive(Debug, Error)]
pub enum StateError {
    #[error("Mongo client cannot be empty")]
    EmptyMongoClient,

    #[error("Redis client cannot be empty")]
    EmptyRedisClient,

    #[error("Http client cannot be empty")]
    EmptyHttpClient,
}
