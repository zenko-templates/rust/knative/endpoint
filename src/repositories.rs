#[cfg(feature = "mongo")]
pub mod mongo;

use axum::async_trait;

use crate::entities::Entity;

pub trait Record {
    fn id(self) -> String;
}

#[async_trait]
pub trait Repository<E: Entity, C> {
    type Config;
    type Record: Record;
    type SearchFilters;

    fn get_entity_type() -> String {
        let (_, entity_type) = std::any::type_name::<E>().rsplit_once(':').unwrap();
        entity_type.to_string()
    }

    async fn get_by_id(&self, id: String) -> Option<E>;
    async fn search(&self, filters: Self::SearchFilters) -> Vec<E>;
    async fn create(&self, entity: E) -> Option<Self::Record>;
    async fn update(&self, entity: E) -> Option<Self::Record>;
    async fn delete(&self, id: String) -> Option<Self::Record>;
}
