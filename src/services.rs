pub mod index;

use std::sync::Arc;

use axum::{routing::IntoMakeService, Router};
use tower_http::trace::{DefaultMakeSpan, DefaultOnResponse, TraceLayer};

use crate::app::state::AppState;

pub fn create_services(state: Arc<AppState>) -> IntoMakeService<Router> {
    let mut router = Router::new().merge(index::create_service(state));

    #[cfg(feature = "cors")]
    {
        use axum::http::Method;
        use tower_http::cors::{Any, CorsLayer};

        let cors = CorsLayer::new()
            .allow_methods([Method::GET, Method::POST])
            // TODO: Update the origin Any is for developpment purpuses only
            .allow_origin(Any);

        router = router.layer(cors);
    }

    router = router.layer(
        TraceLayer::new_for_http()
            .make_span_with(DefaultMakeSpan::default())
            .on_response(DefaultOnResponse::new()),
    );

    router.into_make_service()
}
