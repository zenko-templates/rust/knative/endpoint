pub mod app;
#[cfg(feature = "auth")]
pub mod auth;
#[cfg(feature = "database")]
pub mod database;
#[cfg(feature = "http_client")]
pub mod http_client;
pub mod log;
pub mod services;

use config::{Config, ConfigError, Environment, File};
use serde::Deserialize;

use self::{app::AppSetting, log::LogSetting, services::ServicesSetting};

#[cfg(feature = "auth")]
use self::auth::AuthSetting;
#[cfg(feature = "database")]
use self::database::DatabaseSetting;
#[cfg(feature = "http_client")]
use self::http_client::HttpClientSetting;

#[derive(Debug, Clone, Deserialize)]
pub struct Setting {
    pub app: AppSetting,

    pub log: LogSetting,

    pub services: ServicesSetting,

    #[cfg(feature = "auth")]
    pub auth: AuthSetting,

    #[cfg(feature = "database")]
    pub database: DatabaseSetting,

    #[cfg(feature = "http_client")]
    pub http_client: HttpClientSetting,
}

impl Setting {
    pub fn init() -> Result<Self, ConfigError> {
        Config::builder()
            .add_source(File::with_name("config/default"))
            .add_source(Environment::default())
            .build()?
            .try_deserialize()
    }
}
