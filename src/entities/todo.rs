use serde::{Deserialize, Serialize};

use super::Entity;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Todo {
    #[serde(rename = "_id")]
    pub id: String,
}

impl Entity for Todo {
    fn id(self) -> String {
        self.id
    }
}
