use axum::{http::StatusCode, response::IntoResponse};
use thiserror::Error;

use crate::errors::HttpError;

#[derive(Debug, Error, Clone)]
pub enum IndexError {
    #[error("Failed to connect to the cache")]
    CannotConnectToCache,
    #[error("Failed to write to the cache key: {key}, value: {value}")]
    FailedToWriteToCache { key: String, value: String },
}

impl Into<HttpError> for IndexError {
    fn into(self) -> HttpError {
        let status = match self {
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        };

        HttpError {
            status,
            message: self.to_string(),
        }
    }
}

impl IntoResponse for IndexError {
    fn into_response(self) -> axum::response::Response {
        let error: HttpError = self.into();
        error.into_response()
    }
}
