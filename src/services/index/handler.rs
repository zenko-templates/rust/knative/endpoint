use axum::{
    extract::{Query, State},
    response::Html,
    Json,
};
use handlebars::Handlebars;
use redis::{Commands, SetOptions};
use std::sync::Arc;
use tracing::debug;
use tracing_subscriber::field::debug;

use crate::{
    app::state::AppState,
    entities::todo::Todo,
    extractors::auth::bearer::Bearer,
    repositories::{mongo::MongoRepository, Repository},
};

use super::{dtos, errors::IndexError};

pub async fn handle(
    State(state): State<Arc<AppState>>,
    // bearer: Option<Bearer>,
    // basic: Option<Basic>,
    Query(query): Query<dtos::Query>,
    Json(body): Json<dtos::Body>,
) -> Result<Html<String>, IndexError> {
    debug!("Query: {:?}", query);
    debug!("Body: {:?}", body);
    // debug!("Bearer Auth: {:?}", bearer);
    // debug!("Basic Auth: {:?}", basic);

    let todo_repository = MongoRepository::<Todo>::new(
        state.mongo_client.clone(),
        state.config.database.mongo.todo_collection.clone(),
    );

    let mut cache = state
        .redis_client
        .get_connection()
        .map_err(|_| IndexError::CannotConnectToCache)?;

    let todo = Todo {
        id: "test".to_string(),
    };

    todo_repository.create(todo.clone()).await;

    let key = String::from("key");
    let value = String::from("value");

    cache
        .set_options::<String, String, ()>(
            key.clone(),
            value.clone(),
            SetOptions::default().with_expiration(redis::SetExpiry::EX(30)),
        )
        .map_err(|_| IndexError::FailedToWriteToCache { key, value })?;

    let mut handlebars = Handlebars::new();
    handlebars
        .register_template_file("page", "./public/index.html")
        .unwrap();
    let html = handlebars.render("page", &todo).unwrap();

    Ok(Html(html))
}
