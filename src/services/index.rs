pub mod dtos;
pub mod errors;
pub mod handler;
pub mod response;

use std::sync::Arc;

use axum::{routing::post, Router};

use crate::app::state::AppState;

use handler::handle;

pub fn create_service(state: Arc<AppState>) -> Router {
    Router::new().route("/", post(handle)).with_state(state)
}
