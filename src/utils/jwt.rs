use jsonwebtoken::{errors::Error, Algorithm, DecodingKey, EncodingKey, Header, Validation};
use serde::{de::DeserializeOwned, Serialize};

pub fn decode<T: DeserializeOwned>(key: String, token: String) -> Result<T, Error> {
    let key = DecodingKey::from_rsa_pem(key.as_bytes())?;

    let data = jsonwebtoken::decode::<T>(&token, &key, &Validation::new(Algorithm::RS256))?;

    Ok(data.claims)
}

pub fn encode<T: Serialize>(key: String, claims: T, kid: Option<String>) -> Result<String, Error> {
    let key = match EncodingKey::from_rsa_pem(key.as_bytes()) {
        Ok(key) => key,
        Err(err) => return Err(err),
    };

    let mut header = Header::new(Algorithm::RS256);
    header.kid = kid;

    jsonwebtoken::encode(&header, &claims, &key)
}
