#[cfg(feature = "mongo")]
pub mod mongo;
#[cfg(feature = "redis")]
pub mod redis;

use serde::Deserialize;

#[cfg(feature = "mongo")]
use crate::settings::database::mongo::MongoSetting;
#[cfg(feature = "redis")]
use crate::settings::database::redis::RedisSetting;

#[derive(Debug, Clone, Deserialize)]
pub struct DatabaseSetting {
    #[cfg(feature = "redis")]
    pub redis: RedisSetting,
    #[cfg(feature = "mongo")]
    pub mongo: MongoSetting,
}
