use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct HttpClientSetting {
    pub timeout: Option<u64>,
}
