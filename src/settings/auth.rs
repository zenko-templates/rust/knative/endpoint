use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct AuthSetting {
    #[cfg(feature = "bearer_auth")]
    pub public_key: String,

    #[cfg(feature = "api_key_auth")]
    pub api_key_header: String,
    #[cfg(feature = "api_key_auth")]
    pub api_key: String,
}
