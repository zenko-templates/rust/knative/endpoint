# Knative Rust HTTP Service Starter

A starter to generate Knative http service using rust and warp

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Features](#features)
- [Release](#release)
- [Deploy](#deploy)

## Installation

Clone the project

```sh
git clone git@gitlab.com:zenko-templates/rust/knative/endpoint.git
```

Update the files:
- [ ] The name in the [k8s service file](k8s/service.yaml)

Remove the boilerplate exemple: (Optional)
- [ ] The [index service creation](src/services/index.rs)
- [ ] The [index service](src/routes/index/)
- [ ] The [todo entity](src/entities/todo.rs)
- [ ] The [public index.html](public/index.html)
- [ ] The [useless features](cargo.toml)

Create a new repository

```sh
rm -rf .git
git init
git add .
git commit -m "initial commit"
git remote add origin git@gitlab.com:
git push --all
```

## Usage

To handle a incoming request you only need to modify two file:
- [ ] [service](src/services/index.rs), it contains the route definition
- [ ] [handler](src/services/index/handler.rs), it contains the logic
- [ ] [response](src/services/index/response.rs), it contains the structure used for the response
- [ ] [errors](src/services/index/errors.rs), it contains all the errors of this service
- [ ] [dtos](src/services/index/dtos.rs), it contains all the input of the service

You can add repository like the [mongo repository](src/repositories/mongo.rs) and instanciate it like that if the feature is enable
```rust
let todo_repository = MongoRepository::<Todo>::new(
    state.mongo_client.clone(),
    state.config.database.mongo.todo_collection.clone(),
);
```

You can instantiate redis cache like that if the feature is enable
```rust
let mut cache = match state.redis_client.get_connection() {
    Ok(connection) => connection,
    Err(_) => return Err(()),
};
```

You can use three different extractor for authentication if the respective feature is enable
You can add Option around the extractor to make it infaillible
```rust
pub async fn handle(
    State(state): State<Arc<AppState>>,
    ...,
    Bearer(auth_user): Bearer,
    bearer: Option<Bearer>,
    Basic(auth_user): Basic,
    basic: Option<Basic>,
    ApiKey(api_key): ApiKey,
    api_key: <ApiKey>,
    ...,
) -> Result<Html<String>, ()>
```


## Features

You can activate the following features by setting the name in the default field in the [config file](Cargo.toml)

### Render
- html: Let the ability to use cargo handlebars to template html

### Form
- multipart: Allow the use of multipart-form for the body parsing
- form: Allow the use of x-www-url-encoded for the body parsing

### Database And Services

- redis: Allow the use of redis and all the related config 
- mongo: Allow the use of mongodb and all the related config

### Server
- CORS: Enable cors layer globally 

### Authentication

- jwt: Enable the use of jsonwebtoken
- basic_auth: Add the use of the Basic extractor to validate the credential and get the user
- bearer_auth: Add the use of the Bearer extractor to validate the token and get the user
- api_key_auth: Add the use of the ApiKey extractor to validate and get the api key

## Release

Changelog must be updated before each release using the [git-cliff tool](https://github.com/orhun/git-cliff)


## Deploy

Update the config file:
- [ ] The environment variable in the [k8s service file](k8s/service.yaml)

```sh
kube apply -f k8s/service.yaml
```

